#!/bin/sh

set -e

TARGET_DIR=../qBittorrent

test -d out || mkdir out

GIT_DIR=${TARGET_DIR}/.git GIT_WORK_TREE=${TARGET_DIR}/ git clean -df

p=$1
# patch
echo "patching $p ...";
patch -d "${TARGET_DIR}" -p1 < $p;
GIT_DIR=${TARGET_DIR}/.git GIT_WORK_TREE=${TARGET_DIR}/ git add -A;
GIT_DIR=${TARGET_DIR}/.git GIT_WORK_TREE=${TARGET_DIR}/ git st;
GIT_DIR=${TARGET_DIR}/.git GIT_WORK_TREE=${TARGET_DIR}/ git ci -m "$p";
GIT_DIR=${TARGET_DIR}/.git GIT_WORK_TREE=${TARGET_DIR}/ git st;
GIT_DIR=${TARGET_DIR}/.git GIT_WORK_TREE=${TARGET_DIR}/ git show > out/$p;
ls -lhp out/
