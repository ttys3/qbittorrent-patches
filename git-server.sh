#!/bin/sh
which git-http-backend | GO111MODULE=off go get github.com/asim/git-http-backend
#DOCKER_GW_IP=$(ip -4 addr show docker0 | grep inet | awk '{print $2}' | cut -d'/' -f1)
#for --network host
DOCKER_GW_IP=127.0.0.1
LISTEN_PORT=":8086"
echo "git repo: http://${DOCKER_GW_IP}${LISTEN_PORT}"
git-http-backend -server_address ${LISTEN_PORT} -project_root .