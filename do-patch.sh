#!/bin/sh

set -e

TARGET_DIR=${1:-../qBittorrent/}

test -d out || mkdir out

GIT_DIR=../qBittorrent/.git GIT_WORK_TREE=../qBittorrent/ git reset --hard release-4.5.0
GIT_DIR=../qBittorrent/.git GIT_WORK_TREE=../qBittorrent/ git clean -df


function dopatch() {
    echo "--------------------------------------------------------------------------------"
    p=$1
    echo "patching $p ...";
    patch -d "${TARGET_DIR}" -p1 < $p;
    GIT_DIR=../qBittorrent/.git GIT_WORK_TREE=../qBittorrent/ git add -A;
    GIT_DIR=../qBittorrent/.git GIT_WORK_TREE=../qBittorrent/ git st;
    GIT_DIR=../qBittorrent/.git GIT_WORK_TREE=../qBittorrent/ git ci -m "$p";
    GIT_DIR=../qBittorrent/.git GIT_WORK_TREE=../qBittorrent/ git st;
    GIT_DIR=../qBittorrent/.git GIT_WORK_TREE=../qBittorrent/ git show > out/$p;
    ls -lhp out/
    echo "--------------------------------------------------------------------------------"
}

# patch
echo "begin patch for dir: ${TARGET_DIR}"
for p in *.patch; do
    dopatch $p
done

echo "------------------- all done -------------------"

